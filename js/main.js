//Dome Elements
const time = document.getElementById('time'),
    greeting = document.getElementById('greeting'), 
    name = document.getElementById('name'),
    focus = document.getElementById('focus');

    // Show Time
    function showTime(){
        let today = new Date(),
            hour = today.getHours(),
            min = today.getMinutes(),
            sec = today.getSeconds();

            //Set Am or PM
            const amPm = hour >= 12 ? 'PM' : 'AM';

            //12hr Format
            hour = hour % 12 || 12;

            // Output Time
            time.innerHTML = `${hour}<span>:</span>${addZero(min)}<span>:</span>${addZero(sec)}`;

            // Time out
            setTimeout(showTime, 1000);
    }
function addZero(n){
    return(parseInt(n, 10) < 10 ? '0' : '')+ n;
}
// Set Background and Greeting
function setBgGreet(){
    let today = new Date(),
        hour = today.getHours();
    if(hour < 12){
        //Morning
        document.body.style.backgroundImage = "linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)),url('https://lh3.googleusercontent.com/FPhO6NFfraWR8o7gIu812nsC92O_n5u__9NKUz0YsJetc3M_79KRMR6UwjeI2iJurgdM_Mj81NM6H_cReI3lElGETNBx1aWV89R5iAMDX--ZV7rS4T22YQVUbt9EZeUIoaHrtbvfVMc6Z1p6FzNtIeKrUh9KWBRtdyO5JmmJQZyPUgYncieus7Rk1rArSD4qmNfEJU0JcjnLOctlIxGhPd-HDvNqknxgaC_L9vTbzHQxuTpVn34UXLU8jcdqb2mFGV8y73TxhcqoQh0XdSHEc3CiAaMV5XcitESd_fTK6-cKNZjomJ4eSlGFzFifaKm6KzCmhbGLiu1CSyrXWE3VEQ5I-aXO_IsvHLvIqYTC-PNZNXfGiM09XcbOCYoGK-BmvtjL49wTdHFxFQmiBd9zs6hiiSJev3WJuXasweKIrtmuqisowApJXx_0h_20etqoGusUtxQnB-MZFAXzKClrtgqN2Lu640GrI3QYfa9pColx0pXDwgKl8xdNIBlZziAaKp5EnTAc8ayWBzWcD89itpZmfylvU6bbxYph2eFJt7MljhzIlcEKLDDsYYU2GEdAsu7YffiCB8eM97fCBNY-agj00_s4lcHE5kbKEF0bUd1E5BQrsC_8oPKYlhBVWl3EyvBVHLOOFlYzAlPm6GOHLuXxocsiztTU=w1366-h457-no')";
        greeting.textContent = "Good Morning";
    } else if(hour < 18){
        // Afternoon
        document.body.style.backgroundImage = "linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)),url('https://lh3.googleusercontent.com/4atsELdVEajoKQiMVDuZwQtAD68bHPc8IX45xGhMWMvdPDnb97ickral6m81-r5MRBYqd051d0rJSXMdySZJXw1i2jH56LFK6s2EXgF8UADnUXUOsWcfnqPjC53QAswW83qA9Ys0oMJAEwO7U5A7TTsDYUrLltTQ1A0iiQifG2INTwCHccbd8k8xdHTOeZjcIro2n39_23tjf4Ba5gE-eVr7M3fWQjsXza630Xz9fTw2sOgzRRqEfaePQuhtFuqy8fWf7EZOWLLGroKOZdZnJdIVQMyOPxqE0sxCTGRslQcl0TTcuGxLZXDQMDfrMiD99Y1AE4DqSaFKu84T18lCPJtseM7bg69MW3wLSE_amuDaa9P4SPHb9tSM6ftAszhehqCMUAjIFez8uK0-1LrdVN-AbdiIR_vSK8N7YqXnsSVxSoXIeiLhajAr78O9SjkVdDpSQMC7SO9XofA5J6bW7cnrh5HxfgzPo-zlmRQOjcoUOx2yx-by5T9j8-OIi10r_l3EQsavI_X_hbftXBnH_6ipQpmGoBM8BC80JUVIq5wwCbPSrl2D7O6B5jaWl6jLTyNH3yzSn1hLjMnIoVnSUy_WbmsCkvxVZvN8cHvNU3XnK9u97s8NUt29m3GBiK0nb9aMzIw6sZGbnVOg0wv1JEy-_76hlh7K=w960-h572-no')";
        greeting.textContent = "Good Afternoon";
    } else{
        // Evening
        document.body.style.backgroundImage = "linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)),url('https://lh3.googleusercontent.com/0dPi4Sd34bcFEvJk1BSG39yL6RQaD1M7bCY430d1gidyr0jTZO7byQFVuta4oXmcfQrOv-IkA9f0lRS6otKwywdEfM2iStiFQP4i2RPnw67W3Omsg8z0N_2bjuELlkOM2T5Wd-CaAt5LErmEHeDExJ0GLaeDlsHbrqZJ9xOFkHhng5EK0aZ6ulm6keq0gIGnyQmO90Xg1pX6nUXmARXVdHoVFXDHu7BkAHU8SdvXw_pr5ZYzDzgRgFItJx8iAihVibQhlc-N6q74oyRX7lU5DJaQJeo7EFvMhWFZG_2raymTJ3d2XaHJV2BPoCBI5xZUOq9ttWU0qGAW7tm7LQLcwEuhmdEL8XpIRBgp153C8x0WlLszOMbulIQMYEHKW2BSoc9nWEtLzCpXUJ0wYpSp_ugC-hqDocc_W0_YJtMPan9PWUFuynmWsxF4fYVcSnn6R-d5isXsIv_aKDb-uaXfmowk2NXfCtKakhfBFlRlmLFrKwAaQFHHwyoTENyn6hXtNBo1cdCFfsPtgQWqRnvy_nrEZcNwNjc2AoyPmPCXI_zn1lDIj2zik3HGqNrxLAlSJ6iSdUODe4xpSmbZWCurqrWJ1bj2XCyutaPTkhYuf55YCZDgVDqsEvhLYIsOmvEYFeWNQbBMev3WKCqBkcoAYuWFsljVX1sA=w994-h662-no')";
        greeting.textContent = "Good Evening ";
    }
}
// Run
showTime();
setBgGreet();
